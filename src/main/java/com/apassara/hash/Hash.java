/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.hash;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Hash {
    int A;
    ArrayList<String> ht = new ArrayList<String>();
    
    public Hash(int A){
        this.A = A;
        for(int i = 0; i < A; i++){
            ht.add(i, null);
        }
    }
    private int Hfunc(int f){
        int h = f % A;
        return h;
    }
    public String get(int f){
        int h = Hfunc(f);
        return ht.get(h);
    }
    
    public void put(int f, String p){
        int h = Hfunc(f);
        ht.set(h, p);
    }
    
    public void remove(int f){
        int h = Hfunc(f);
         ht.set(h, null);
    }
    
    public void Allput(){
        for(int i = 0; i < A; i++){
            System.out.println("Key : " + i + " Value : " + ht.get(i)); 
        }
    }
    
    
}
